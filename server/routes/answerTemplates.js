'use strict';

// Answer templates routes use AnswerTemplates  controller
var templates = require('../controllers/answerTemplates');

module.exports = function (app) {

    app.route('/templates')
        .get(templates.templates)
        .post(templates.create);
    app.route('/templates/category/:category')
        .get(templates.templatesByCategory);
    app.route('/templates/:templateId')
        .get(templates.template)
        .put(templates.update)
        .delete(templates.remove);
    app.param('templateId', templates.template);
    app.param('templateId', templates.update);
    app.param('templateId', templates.remove);
};

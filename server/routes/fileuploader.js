'use strict';


module.exports = function (app) {

    app.route('/uploadimage')
        .post(function (req, res) {
            var files = req.body.files;
            console.log(files);
            res.send('ok');
            //File upload logic here
            //Make sure to delete or move the file accordingly here, otherwise files will pile up in `/tmp`
        });

};

'use strict';

// Answers routes use Answers controller
var filectrl = require('../controllers/fileupload');

module.exports = function (app) {

    app.route('/api/fileupload')
        .post(filectrl.fileupload);

};

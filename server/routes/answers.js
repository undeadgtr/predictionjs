'use strict';

// Answers routes use Answers controller
var answers = require('../controllers/answers');

module.exports = function (app) {

    app.route('/answers/create')
        .post(answers.create);

    app.route('/answers/:id')
        .get(answers.answer)
        .put(answers.update);

    app.param('id', answers.answer);
    app.param('id', answers.update);

    app.route('/answers/:category/:amount')
        .get(answers.lastAnswers);

    app.route('/answers/:category/:amount/:answerId')
        .get(answers.lastAnswersBefore);
};

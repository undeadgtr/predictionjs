'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Answer Template Schema
 */
var AnswerTemplateSchema = new Schema({
    category: {
        type: String,
        required: true
    },
    answer: {
        type: String,
        required: true
    },
    imageUrl: {
        type: String,
        required: false
    },
    keywords: {
        type: Array,
        required: false,
        default: []
    },
    createdDate: {
        type: Date,
        required: true
    },
    modifiedDate: {
        type: Date,
        required: true,
        default: Date.now
    }
});


/**
 * Methods
 */
AnswerTemplateSchema.methods = {
};

mongoose.model('AnswerTemplate', AnswerTemplateSchema);

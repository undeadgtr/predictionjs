'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Answer Schema
 */
var AnswerSchema = new Schema({
    category: {
        type: String,
        required: true
    },
    question: {
        type: String,
        required: false
    },
    answer: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: false
    },
    partner: {
        type: String,
        required: false
    },
    variants: {
        type: Array,
        required: false,
        default: []
    },
    imageUrl: {
        type: String,
        required: false
    },
    best: {
        type: Boolean,
        required: true,
        default: false
    },
    createdDate: {
        type: Date,
        required: true
    },
    modifiedDate: {
        type: Date,
        required: true,
        default: Date.now
    }
});


/**
 * Methods
 */
AnswerSchema.methods = {

    isBest: function () {
        return this.best;
    }

};

mongoose.model('Answer', AnswerSchema);

'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    AnswerTemplate = mongoose.model('AnswerTemplate');

var saveOrCreate = function (template, req, res, next) {
    template.save(function (err) {
        if (err) {
            return res.status(400);
        }
        res.send({template: template});
    });
};


/**
 * Create Template with field answer
 */
exports.create = function (req, res, next) {
    var template = new AnswerTemplate(req.body);

    req.assert('category', 'You must enter a category').notEmpty();
    req.assert('imageUrl', 'You must enter an imageUrl').notEmpty();
    req.assert('answer', 'You must enter an answer').notEmpty();
    req.assert('keywords', 'You must enter at least one keyword').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        return res.status(400).send(errors);
    }

    var keywords = template.keywords;
    var newKeywords = [];
    function toLower(element, index, array) {
        newKeywords.push(element.toLowerCase());
    }
    keywords.forEach(toLower);

    template.keywords = newKeywords;


    template.createdDate = Date.now();
    saveOrCreate(template, req, res, next);
};

/**
 * Update existing template.
 */
exports.update = function (req, res, next) {
    var template = new AnswerTemplate(req.body);

    req.assert('_id', '_id must be not empty before update').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        return res.status(400).send(errors);
    }
    saveOrCreate(template, req, res, next);
};


/**
 * Find template by id
 */
exports.template = function (req, res, next, id) {
    AnswerTemplate
        .findOne({_id: id})
        .exec(function (err, template) {
            if (err) return res.status(400).send(err);
            if (!template) return res.status(404).send('Answer Template with ' + id + ' not found.');
            res.send({template: template});
        });
};

/**
 * Find all  templates
 */
exports.templates = function (req, res) {
    AnswerTemplate
        .find({})
        .exec(function (err, templates) {
            if (err) return res.status(400).send(err);
            res.send({templates: templates});
        });
};

/**
 * Find templates by category
 */
exports.templatesByCategory = function (req, res) {
    var category = req.params.category;
    AnswerTemplate
        .find({category: category})
        .exec(function (err, templates) {
            if (err) return res.status(400).send(err);
            res.send({templates: templates});
        });
};

/**
 * Remove template by id
 */
exports.remove = function (req, res, next, id) {
    AnswerTemplate
        .findByIdAndRemove(id)
        .exec(function (err, template) {
            if (err) return next(err);
            if (!template) return next(new Error('Failed to load Answer Template' + id));
            res.send({'removed': template});
        });
};
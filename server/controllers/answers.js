'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Answer = mongoose.model('Answer'),
    AnswerTemplate = mongoose.model('AnswerTemplate');


var knownKeywords = 'кто что какой чей сколько который где откуда почему зачем как когда'.split(' ');

var getMainKeyword = function(str){
    var words = str
        .toLowerCase()
        .replace('.', '')
        .replace(',', '')
        .replace('!', '')
        .replace('?', '')
        .replace(':', '')
        .replace('-', '')
        .split(' ');
    var mainKeyWord = '';
    words.forEach(function(element){
        if(knownKeywords.indexOf(element) >  -1){
            mainKeyWord = element;
            return;
        }
    });
    return mainKeyWord;
};

var saveOrCreate = function (answer, req, res, next) {
    answer.save(function (err) {
        if (err) {
            return res.status(400);
        }
        res.send({answer: answer});
    });
};

var setAnswerAndImageProperty = function(answer,req, res, next, category, count){
    if(count === 0){
        answer.answer = 'Мы еще не знаем ответа на Ваш вопрос :(';
        saveOrCreate(answer, req, res, next);
    } else {
        var rand = Math.floor(Math.random() * count);
        AnswerTemplate
            .findOne({category: category})
            .skip(rand)
            .exec(function (err, template) {
                if (err) return res.status(400).send(err);
                answer.answer = template.answer;
                answer.imageUrl = template.imageUrl;
                saveOrCreate(answer, req, res, next);
            });
    }
};

var setAnswerAndImagePropertyForGeneral = function(answer,req, res, next, category, count, mainKeyWord){
    if(mainKeyWord === ''){
        answer.answer = 'Мы еще не знаем ответа на Ваш вопрос :(';
        saveOrCreate(answer, req, res, next);
    } else if(count === 0){
        answer.answer = 'Мы еще не знаем ответа на Ваш вопрос :(';
        saveOrCreate(answer, req, res, next);
    } else {
        var rand = Math.floor(Math.random() * count);
        AnswerTemplate
            .findOne({category: category, keywords: mainKeyWord})
            .skip(rand)
            .exec(function (err, template) {
                if (err) return res.status(400).send(err);
                answer.answer = template.answer;
                answer.imageUrl = template.imageUrl;
                saveOrCreate(answer, req, res, next);
            });
    }
};

var setImageProperty = function(answer,req, res, next, category, count){
    if(count === 0){
        answer.answer = 'Мы еще не знаем ответа на Ваш вопрос :(';
        saveOrCreate(answer, req, res, next);
    } else {
        var rand = Math.floor(Math.random() * count);
        AnswerTemplate
            .findOne({category: category})
            .skip(rand)
            .exec(function (err, template) {
                if (err) return res.status(400).send(err);
                answer.imageUrl = template.imageUrl;
                saveOrCreate(answer, req, res, next);
            });
    }
};

var fillAnswer = function (answer, req, res, next) {
    var category = answer.category;
    answer.createdDate = Date.now();
    if(category === 'general'){
        var question = answer.question;
        var mainKeyWord = getMainKeyword(question);
        AnswerTemplate
            .count({category: category, keywords: mainKeyWord}, function (err, count) {
                if (err) return res.status(400).send(err);
                setAnswerAndImagePropertyForGeneral(answer, req, res, next, category, count, mainKeyWord);
            });

    } else if(category === 'choice'){
        var variants = answer.variants;
        var rand = Math.floor(Math.random() * variants.length);
        var answerStr = variants[rand];
        answer.answer = answerStr;
        AnswerTemplate
            .count({category: category}, function (err, count) {
                if (err) return res.status(400).send(err);
                setImageProperty(answer, req, res, next, category, count);
            });
    }  else {
        AnswerTemplate
            .count({category: category}, function (err, count) {
                if (err) return res.status(400).send(err);
                setAnswerAndImageProperty(answer, req, res, next, category, count);
            });
    }
};

/**
 * Create Answer with field answer
 */
exports.create = function (req, res, next) {
    var answer = new Answer(req.body);

    req.assert('category', 'You must enter a category').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        return res.status(400).send(errors);
    }
    fillAnswer(answer, req, res, next);

};

/**
 * Update existing answer.
 */
exports.update = function (req, res, next) {
    var answer = new Answer(req.body);

    req.assert('_id', '_id must be not empty before update').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        return res.status(400).send(errors);
    }
    saveOrCreate(answer, req, res, next);
};


/**
 * Find answer by id
 */
exports.answer = function (req, res, next, id) {
    Answer
        .findOne({_id: id})
        .exec(function (err, answer) {
            if (err) return next(err);
            if (!answer) return res.status(404).send('Answer with ' + id + ' not found.');
            res.send({answer: answer});
        });
};

/**
 * Get the last answers.
 */
exports.lastAnswers = function (req, res) {
    var category = req.params.category;
    var amount = req.params.amount;
    Answer
        .find({category: category})
        .limit(amount)
        .sort('-_id')
        .exec(function (err, answers) {
            if (err) return res.status(400).send(err);
            res.send({answers: answers});
        });
};
/**
 * Get the previous answers before
 */
exports.lastAnswersBefore = function (req, res) {
    var id = req.params.answerId;
    var category = req.params.category;
    var amount = req.params.amount;
    Answer
        .find({category: category})
        .where('_id').lt(id)
        .limit(amount)
        .sort('-_id')
        .exec(function (err, answers) {
            if (err) return res.status(400).send(err);
            res.send({answers: answers});
        });
};



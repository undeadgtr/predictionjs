'use strict';

angular.module('mean.controllers.prediction', [])

    .controller('MainCtrl', ['$scope', '$rootScope', '$parse', '$location', '$anchorScroll', '$http', 'SocialMaker', 'PredictionConfig',
        function ($scope, $rootScope, $parse, $location, $anchorScroll, $http, SocialMaker, PredictionConfig) {

            $rootScope.categoryIds = {};
            $scope.answers = [];

            $scope.init = function () {
                $scope.selectedQuestion = 'question';
                $rootScope.topCategory = 'general';

                PredictionConfig.categories.forEach(function (category) {
                    $http.get('/answers/' + category.name + '/6')
                        .success(function (responce) {
                            if (responce.answers.length > 0) {
                                $scope.answers = $scope.answers.concat(SocialMaker.buildAll(responce.answers));
                                $rootScope.categoryIds[category.name] = responce.answers[responce.answers.length - 1]._id;
                            }
                        })
                        .error(function () {
                            $scope.loadAnswerError = 'Load answer failed.';
                        });
                });
            };
            $scope.init();

            $scope.closeAnswer = function () {
                $rootScope.answer = null;
            };

            $scope.addAnswers = function () {
                var category = $rootScope.topCategory;
                $http.get('/answers/' + category + '/6/' + $rootScope.categoryIds[category])
                    .success(function (responce) {
                        if (responce.answers.length > 0) {
                            $scope.answers = $scope.answers.concat(SocialMaker.buildAll(responce.answers));
                            $rootScope.categoryIds[category] = responce.answers[responce.answers.length - 1]._id;
                        }
                    })
                    .error(function () {
                        $scope.loginerror = 'Authentication failed.';
                    });
            };

            $scope.setTopCategory = function (type) {
                $rootScope.topCategory = type;
            };


//            function hideByVal(val) {
//                var model = $parse(val);
//                model.assign($scope, true);
//            }

            $scope.$on('$routeChangeStart', function (next, current) {
                $location.hash('top');
                $anchorScroll();
            });

            $rootScope.$on('relation:error', function (event, data) {
                var buffer = '';

                angular.forEach(data.errors, function (error) {
                    if (typeof error === 'object') {
                        angular.forEach(error, function (err) {
                            buffer = buffer + err;
                        });
                    } else {
                        buffer = buffer + error;
                    }
                });

                alert('Ошибка при работе с юзерами: ' + buffer);
            });

        }
    ])

    .controller('GeneralCtrl', ['$scope', '$rootScope', '$http', 'SocialMaker',
        function ($scope, $rootScope, $http, SocialMaker) {

            $scope.getAnswer = function () {
                if ($scope.question) {
                    $rootScope.answer = null;
                    $scope.answererror = null;
//                    var answer = {};
//                    var question = $scope.question.trim();

                    $http.post('/answers/create', {
                        question: $scope.question.trim(),
                        category: 'general'
                    })
                        .success(function (response) {
                            //$scope.loginError = 0;
                            $rootScope.answer = SocialMaker.build(response.answer);
                            //$rootScope.$emit('loggedin');
                        })
                        .error(function () {
                            $scope.answererror = 'Answer failed.';
                        });
                }
            };

        }
    ])

    .controller('AffirmativeCtrl', ['$scope', '$rootScope', '$http', 'SocialMaker',
        function ($scope, $rootScope, $http, SocialMaker) {

            $scope.getAnswer = function () {
                if ($scope.question) {
                    $rootScope.answer = null;
                    $scope.answererror = null;
                    $http.post('/answers/create', {
                        question: $scope.question.trim(),
                        category: 'affirmative'
                    })
                        .success(function (response) {
                            $rootScope.answer = SocialMaker.build(response.answer);
                        })
                        .error(function () {
                            $scope.answererror = 'Answer failed.';
                        });
                }
            };
        }
    ])

    .controller('ChoiceCtrl', ['$scope', '$rootScope', '$http', 'SocialMaker',
        function ($scope, $rootScope, $http, SocialMaker) {

            $scope.variants = [
                { value: '' },
                { value: '' }
            ];

            $scope.addVariant = function () {
                $scope.variants.push({ value: '' });
            };

            $scope.getAnswer = function () {

                $rootScope.answer = null;
                $scope.answererror = null;

                var variantsList = [];
                $scope.variants.forEach(function (variant) {
                    variantsList.push(variant.value);
                });

                $http.post('/answers/create', {
                    variants: variantsList,
                    category: 'choice'
                })
                    .success(function (response) {
                        $rootScope.answer = SocialMaker.build(response.answer);
                    })
                    .error(function () {
                        $scope.answererror = 'Answer failed.';
                    });

            };

        }
    ])
    .controller('RelationCtrl', ['$scope', '$rootScope', '$http', 'SocialMaker',
        function ($scope, $rootScope, $http, SocialMaker) {

            $scope.getAnswer = function () {
                if ($scope.name) {
                    $rootScope.answer = null;
                    $scope.answererror = null;

                    $http.post('/answers/create', {
                        name: $scope.name.trim(),
                        partner: $scope.partner.trim(),
                        category: 'relation'
                    })
                        .success(function (response) {
                            $rootScope.answer = SocialMaker.build(response.answer);
                        })
                        .error(function () {
                            $scope.answererror = 'Answer failed.';
                        });
                }
            };

//            $scope.getAnswer = function () {
//                if ($scope.question) {
//                    var answer = {};
//
//                    var question = $scope.question.trim();
//
//                    answer.question = question;
//                    answer.answer = 'Да, это возможно';
//                    answer.imageUrl = '/public/prediction/assets/img/img_1.jpg';
//                    $rootScope.answer = answer;
//                    $rootScope.selectAnswer('asked');
//                }
//            };

        }
    ])
    .controller('LongLiveCtrl', ['$scope', '$rootScope', '$http', 'SocialMaker',
        function ($scope, $rootScope, $http, SocialMaker) {

            $scope.getAnswer = function () {
                if ($scope.name) {
                    $rootScope.answer = null;
                    $scope.answererror = null;

                    $http.post('/answers/create', {
                        name: $scope.name.trim(),
                        category: 'longlive'
                    })
                        .success(function (response) {
                            $rootScope.answer = SocialMaker.build(response.answer);
                        })
                        .error(function () {
                            $scope.answererror = 'Answer failed.';
                        });
                }
            };

        }
    ])
    .controller('NumberCtrl', ['$scope', '$rootScope', '$http', 'SocialMaker',
        function ($scope, $rootScope, $http, SocialMaker) {

            $scope.getAnswer = function () {
                if ($scope.name) {
                    $rootScope.answer = null;
                    $scope.answererror = null;

                    $http.post('/answers/create', {
                        name: $scope.name.trim(),
                        category: 'number'
                    })
                        .success(function (response) {
                            $rootScope.answer = SocialMaker.build(response.answer);
                        })
                        .error(function () {
                            $scope.answererror = 'Answer failed.';
                        });
                }
            };

        }
    ]);



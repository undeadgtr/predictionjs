'use strict';

//Setting up route
angular.module('mean.prediction').config(['$stateProvider',

    function($stateProvider) {

        $stateProvider
            .state('prediction.general', {
                url: '/general',
                templateUrl: 'public/prediction/views/questions/general.html'
            })
            .state('prediction.affirmative', {
                url: '/affirmative',
                templateUrl: 'public/prediction/views/questions/affirmative.html'
            })
            .state('prediction.choice', {
                url: '/choice',
                templateUrl: 'public/prediction/views/questions/choice.html'
            })
            .state('prediction.relation', {
                url: '/relation',
                templateUrl: 'public/prediction/views/questions/relation.html'
            })
            .state('prediction.longlive', {
                url: '/longlive',
                templateUrl: 'public/prediction/views/questions/longlive.html'
            })
            .state('prediction.number', {
                url: '/number',
                templateUrl: 'public/prediction/views/questions/number.html'
            });
    }

]);

'use strict';

angular.module('mean.factory.prediction', [])
    .factory('SocialMaker', ['PredictionConfig', function (PredictionConfig) {

        function buildSocial (answer){
            if(answer.category === 'general' || answer.category === 'affirmative'){
                answer.socQuestion = 'Ваш вопрос: ' + answer.question;
                answer.socAnswer = 'Ваш ответ: ' + answer.answer;
            } else if(answer.category === 'choice'){
                answer.socQuestion = 'Варианты: ' + answer.variants.join(',');
                answer.socAnswer = 'Ваш вариант: ' + answer.question;
            } else if(answer.category === 'relation'){
                answer.socQuestion = 'Вопрос о ваших отношениях';
                answer.socAnswer = 'Между ' + answer.name + ' и ' + answer.partner + ': ' + answer.answer;
            } else if(answer.category === 'longlive'){
                answer.socQuestion = 'Сколько мне осталось жить?';
                answer.socAnswer = 'Вам, ' + answer.name + ', оталось жить: ' + answer.answer;
            } else if(answer.category === 'number'){
                answer.socQuestion = 'Мое счастливое число?';
                answer.socAnswer = 'Ваше счастливое число, ' + answer.name + ': ' + answer.answer;
            }

            answer.socUrl = PredictionConfig.url;
            return answer;
        }

        return {
            'build': function (answer) {
                return buildSocial(answer);
            },
            'buildAll': function (answers) {
                var newAnswers = [];
                answers.forEach(function(answer){
                    newAnswers.push(buildSocial(answer));
                });
                return buildSocial(newAnswers);
            }
        };
    }
    ]);

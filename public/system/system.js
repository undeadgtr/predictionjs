'use strict';

angular.module('mean.system', ['mean.controllers.login', 'mean.controllers.prediction', 'mean.controllers.admin', 'mean.factory.prediction', 'mean-factory-interceptor']);
'use strict';

angular.module('mean.controllers.admin', [])

    .controller('AdminCtrl', ['$scope', '$rootScope', '$parse', '$location', '$anchorScroll',
        function ($scope, $rootScope, $parse, $location, $anchorScroll) {

            $scope.init = function () {
                $scope.selectedQuestion = 'question';
                $rootScope.selectedAnswer = 'default';
                $rootScope.topType = 'best';
            };
            $scope.init();

            var question = 'Как жить дальше?',
                image = '/public/prediction/assets/img/img_5.jpg',
                date = '25 мая 2014',
                answer = 'Нормально живи!';

            $scope.items = [];

            $scope.addItem = function () {
                $scope.items.push({
                    question: question,
                    image: image,
                    type: $rootScope.topType,
                    date: date,
                    answer: answer
                });
            };

            $rootScope.selectAnswer = function (item) {
                $rootScope.selectedAnswer = item;
            };

            $rootScope.isSelectedAnswer = function (item) {
                return $rootScope.selectedAnswer === item;
            };

            $scope.setTopType = function (type) {
                $rootScope.topType = type;
            };


//            function hideByVal(val) {
//                var model = $parse(val);
//                model.assign($scope, true);
//            }

            $scope.$on('$routeChangeStart', function (next, current) {
                $location.hash('top');
                $anchorScroll();
            });

            $rootScope.$on('relation:error', function (event, data) {
                var buffer = '';

                angular.forEach(data.errors, function (error) {
                    if (typeof error === 'object') {
                        angular.forEach(error, function (err) {
                            buffer = buffer + err;
                        });
                    } else {
                        buffer = buffer + error;
                    }
                });

                alert('Ошибка при работе с юзерами: ' + buffer);
            });

        }
    ])

    .controller('AdminAnswersCtrl', ['$scope', '$rootScope', '$http', '$location', 'PredictionConfig', '$fileUploader',
        function ($scope, $rootScope, $http, $location, PredictionConfig, $fileUploader) {
            $scope.categories = PredictionConfig.categories;
            $scope.selectedCategory = $scope.categories[0];

            $scope.templates = [];
            $scope.keywords = [];
            $scope.imageUrl = null;

            var uploader = $scope.uploader = $fileUploader.create({
                scope: $scope,
                url: '/api/fileupload'
            });

            $scope.init = function () {
                $scope.templates = [];
                $http.get('/templates/category/' + $scope.selectedCategory.name)
                    .success(function (responce) {
                        if (responce.templates.length > 0) {
                            $scope.templates = responce.templates;
                        }
                    })
                    .error(function () {
                        $scope.loadAnswerError = 'Load answer failed.';
                    });
            };
            $scope.init();

            $scope.onSelect = function () {
                $scope.init();
            };

            $scope.addAnswer = function () {
                $http.post('/templates', {
                    category: $scope.selectedCategory.name,
                    answer: $scope.answerForm.trim(),
                    imageUrl: $scope.imageUrl,
                    keywords: $scope.keywords.map(function (item) {
                        return item.text;
                    })
                })
                    .success(function (response) {
                        $scope.templates.push(response.template);
                        $scope.keywords = [];
                        $scope.answerForm = null;
                        $scope.imageUrl = null;
                        uploader.clearQueue();
                    })
                    .error(function () {
                        $scope.answererror = 'Answer failed.';
                    });
            };


            // Images only FILTER
            uploader.filters.push(function (item /*{File|HTMLInputElement}*/) {
                var type = uploader.isHTML5 ? item.type : '/' + item.value.slice(item.value.lastIndexOf('.') + 1);
                type = '|' + type.toLowerCase().slice(type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            });


            // REGISTER HANDLERS

            uploader.bind('afteraddingfile', function (event, item) {
                //console.info('After adding a file', item);
            });

            uploader.bind('afterremovingfile', function (event, item) {
                $scope.imageUrl = null;
            });

            uploader.bind('whenaddingfilefailed', function (event, item) {
                //console.info('When adding a file failed', item);
            });

            uploader.bind('afteraddingall', function (event, items) {
                //console.info('After adding all files', items);
            });

            uploader.bind('beforeupload', function (event, item) {
                //console.info('Before upload', item);
            });

            uploader.bind('progress', function (event, item, progress) {
                //console.info('Progress: ' + progress, item);
            });

            uploader.bind('success', function (event, xhr, item, response) {
                //console.info('Success', xhr, item, response);
            });

            uploader.bind('cancel', function (event, xhr, item) {
                //console.info('Cancel', xhr, item);
            });

            uploader.bind('error', function (event, xhr, item, response) {
                //console.info('Error', xhr, item, response);
            });

            uploader.bind('complete', function (event, xhr, item, response) {
                //console.info('Complete', xhr, item, response);
                $scope.imageUrl = response.path;
            });

            uploader.bind('progressall', function (event, progress) {
                //console.info('Total progress: ' + progress);
            });

            uploader.bind('completeall', function (event, items) {
                //console.info('Complete all', items);
            });


        }
    ]);



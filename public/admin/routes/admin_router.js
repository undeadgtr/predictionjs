'use strict';

//Setting up route
angular.module('mean.admin').config(['$stateProvider',

    function($stateProvider) {

        $stateProvider
            .state('admin.answers', {
                url: '/admin/answers',
                templateUrl: 'public/admin/views/answers.html'
            });
    }

]);
